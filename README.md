OSM2Tempus
==========

OSM2Tempus is a tool dedicated to the import of OpenStreetMap data for routing with the [Tempus](http://ifsttar.github.io/Tempus/) framework.
